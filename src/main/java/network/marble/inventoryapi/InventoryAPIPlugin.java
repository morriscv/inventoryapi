package network.marble.inventoryapi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import network.marble.inventoryapi.api.SearchingMenu;
import network.marble.inventoryapi.inventories.ArmorSet;
import network.marble.inventoryapi.inventories.Inventory;
import network.marble.inventoryapi.inventories.Menu;
import network.marble.inventoryapi.itemstacks.InventoryItem;
import network.marble.inventoryapi.listeners.InventoryListener;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class InventoryAPIPlugin extends JavaPlugin {
    private static InventoryAPIPlugin plugin;
    private static InventoryConfig config;
    public static Connection conn = null;
    public static String serverID;
    public static JedisPool pool;
    
    public static Map<Integer,Inventory> inventories = new HashMap<Integer,Inventory>();
    public static Map<Integer,InventoryItem> globalItems = new HashMap<Integer,InventoryItem>();
    public static Map<UUID,Integer> playerInventories = new HashMap<UUID,Integer>();
    public static Map<UUID,Menu> playerCurrentMenus = new HashMap<UUID,Menu>();
    public static Map<UUID,Long> playerLastItemUse = new HashMap<UUID,Long>();
    public static Map<UUID,ArmorSet> playerArmor = new HashMap<UUID,ArmorSet>();
    
    @Override
    public void onEnable() {
    	if(isDataManagerPresent()){
	        plugin = this;
	        
	        Inventory empty = new Inventory(0);
	        inventories.put(0, empty);
	        
	        initConfig(false);
	        setupJedisPool();
	        prepareMySQL();
	        if(config.enableInventoryProtection){
	        	registerEvents();
	        }
	        new SearchingMenu();
	        playerDataLossRepair();
	        //genTestData();
	        getLogger().info("InventoryAPI successfully loaded.");
    	}else{
    		getLogger().severe("Data Manager is not present. Disabling.");
    		setEnabled(false);
    	}
    }


	@Override
    public void onDisable(){
    	try {
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	pool.destroy();
    }
    
   

	private void initConfig(boolean repeat) {
    	try(Reader reader = new FileReader(this.getDataFolder() + "/config.json")){
			config = new Gson().fromJson(reader, InventoryConfig.class);
    		return;
		} catch (Exception e){
			getLogger().info("Generating config.");
		}
    	InventoryConfig builder = new InventoryConfig(); 
    	File directory = this.getDataFolder();
		
    	if (directory.exists() == false){
			try{
				directory.mkdir();
			}catch(Exception e){
				if(repeat){
					this.setEnabled(false);
				}
			}
		}
		
		try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.getDataFolder() + "/config.json")))){
			Gson file = new GsonBuilder().create();
			file.toJson(builder, writer);
			getLogger().info("Config file has been created");
		} catch (Exception e){
			e.printStackTrace();
			if(repeat){
				this.setEnabled(false);
			}
		}
		if(!repeat){
			initConfig(true);
		}
	}
    
    private void setupJedisPool(){
    	try{
	    	JedisPoolConfig poolcfg = new JedisPoolConfig();
	        poolcfg.setMaxTotal(getServer().getMaxPlayers() + 1);//Everyone can disconnect simultaneously
	        pool = new JedisPool(new JedisPoolConfig(), config.redisHost, config.redisPort);
    	}catch(Exception e){
    		getLogger().severe("Failed to load at Redis setup.");
            getLogger().severe(e.getMessage());
    		setEnabled(false);
    	}
    }
    
    private void prepareMySQL(){
    	try{
    		conn = DriverManager.getConnection("jdbc:mysql://" + config.sqlHost + ":" + config.sqlPort + "/MarbleNetwork_Queues","QueuePlugin","todoSecurity");//TODO dynamic database connecting
    	}catch (SQLException e){
    		getLogger().severe("Failed to load at MySQL connection initialisation.");
            getLogger().severe(e.getMessage());
            setEnabled(false);
    	}
    }
    
    public static void registerEvents(){
    	plugin.getServer().getPluginManager().registerEvents(new InventoryListener(), plugin);
    }
    
    public static void unregisterEvents(){
    	HandlerList.unregisterAll(plugin);
    }
    
    private void playerDataLossRepair() {
    	for (Player player : getServer().getOnlinePlayers()) {
    		playerLastItemUse.putIfAbsent(player.getUniqueId(), 0L);
            player.closeInventory();
        }
	}
    
    public static InventoryAPIPlugin getPlugin() {
        return plugin;
    }
    
    public static InventoryConfig getConfigModel() {
        return config;
    }

    public static boolean isDataManagerPresent(){ 
        return Bukkit.getPluginManager().getPlugin("MRN-DataStorageManager") != null;
    } //Return true if the DataStorageManager is found, and therefore it is safe to use functionality from it, or false otherwise.
}
