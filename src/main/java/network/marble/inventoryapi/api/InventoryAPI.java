package network.marble.inventoryapi.api;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import network.marble.inventoryapi.itemstacks.InventoryItem;

import network.marble.inventoryapi.InventoryAPIPlugin;
import network.marble.inventoryapi.enums.ArmorType;
import network.marble.inventoryapi.interfaces.ItemStackGetter;
import network.marble.inventoryapi.inventories.ArmorSet;
import network.marble.inventoryapi.inventories.Inventory;
import network.marble.inventoryapi.inventories.Menu;
import network.marble.inventoryapi.inventories.SubMenu;
import network.marble.inventoryapi.itemstacks.ActionItemStack;
import network.marble.inventoryapi.itemstacks.PlayerSearchInvokingItemStack;
import network.marble.inventoryapi.itemstacks.SubMenuInvokingItemStack;

public class InventoryAPI {

	/***
	 * Sets whether item pickup from ground items and player movement of items around inventory menus is enabled.
	 * @param flag True will enable item pickup.
	 */
	public static void setItemPickup(boolean flag){
		if(flag){
			InventoryAPIPlugin.unregisterEvents();
		}else{
			InventoryAPIPlugin.registerEvents();
		}
	}
	
	/***
	 * Adds a new inventory to the inventories list of this API.
	 * Inventorys should be created by a central plugin before being modified by additional plugins.
	 * @return Integer id of the newly created inventory.
	 */
	public static int createNewInventory(){
		int id = InventoryAPIPlugin.inventories.size() - 1;//-1 accounts for internal SearchingMenu inventory
		
		Inventory inv = new Inventory(id);
		InventoryAPIPlugin.inventories.put(id, inv);
		return id;
	}
	
	public static int duplicateInventory(int id){
		Inventory inv = InventoryAPIPlugin.inventories.get(id);
		
		ItemStack[] realInvItems = new ItemStack[inv.getRealInventory().length];
		
		for(int i = 0; i < inv.getRealInventory().length; ++i){
			if(inv.getRealInventory()[i] != null){
				realInvItems[i] = inv.getRealInventory()[i].clone();
			}
		}
		
		int newID = createNewInventory();
		
		for(int i = 0; i < inv.getAllInventoryItems().length; ++i){
			InventoryItem existingII = inv.getAllInventoryItems()[i];
			if(existingII instanceof ActionItemStack){
				ActionItemStack existingAIS = (ActionItemStack)existingII;
				addItemToInventory(newID, realInvItems[i], getPlayerInventorySlotXPosition(i), getPlayerInventorySlotYPosition(i), existingAIS.getCommand(), existingAIS.requiresConfirmation());
			}else if(existingII instanceof PlayerSearchInvokingItemStack){
				PlayerSearchInvokingItemStack existingPSIIS = (PlayerSearchInvokingItemStack)existingII;
				if(existingPSIIS.isRedis()){
					addItemToInventory(newID, realInvItems[i], getPlayerInventorySlotXPosition(i), getPlayerInventorySlotYPosition(i), existingPSIIS.getRawCommand(), existingPSIIS.getReplaceString(), existingPSIIS.getsetName(), existingPSIIS.doesExcludeViewerUUID(), existingPSIIS.requiresConfirmation());
				}else{
					addItemToInventory(newID, realInvItems[i], getPlayerInventorySlotXPosition(i), getPlayerInventorySlotYPosition(i), existingPSIIS.getRawCommand(), existingPSIIS.getReplaceString(), existingPSIIS.getSQLString(), existingPSIIS.getsetName(), existingPSIIS.usesOriginCode(), existingPSIIS.requiresConfirmation());
				}
			}else if(existingII instanceof SubMenuInvokingItemStack){
				SubMenuInvokingItemStack existingSMIIS = (SubMenuInvokingItemStack)existingII;
				addItemToInventory(newID, existingSMIIS.getItemStack(null), getPlayerInventorySlotXPosition(i), getPlayerInventorySlotYPosition(i), existingSMIIS.getInventorySize(), existingSMIIS.getContainedItems(), existingSMIIS.getInventoryName());
			}
		}
		
		return newID;
	}
	
	public static void duplicateAllInvetories(){
		Iterator<Entry<Integer, Inventory>> it = InventoryAPIPlugin.inventories.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Inventory> key = (Map.Entry<Integer, Inventory>)it.next();
	        int keyData = key.getKey();
	        if(keyData >= 0){
	        duplicateInventory(key.getKey());
	        }
	    }
	}
	
	public static ItemStack[] getInventoryItemStacks(int inventoryID){
		return InventoryAPIPlugin.inventories.get(inventoryID).getRealInventory();
	}
	
	/***
	 * Closes the inventory being used by a player and does cleanup.
	 * @param player The player who will have their currently open (if at all) inventory screen closed.
	 */
	public static void closePlayerCurrentMenu(Player player){
		player.closeInventory();
		InventoryAPIPlugin.playerCurrentMenus.remove(player.getUniqueId());
	}
	
	/***
	 * Sets the input player's InventoryAPI inventory id to the input id and updates their inventory to display this inventory's items.
	 * @param player The player to change the inventory of.
	 * @param inventoryID The ID of the InventoryAPI inventory.
	 * @return
	 */
	public static boolean setPlayerInventoryType(Player player, int inventoryID){
		if(InventoryAPIPlugin.inventories.get(inventoryID) != null){
			if(player != null){
				Inventory inv = InventoryAPIPlugin.inventories.get(inventoryID);
				ItemStack[] realItemStacks = inv.getRealInventory();
				
				for(Entry<Integer, InventoryItem> ii:InventoryAPIPlugin.globalItems.entrySet()){
					int slot = ii.getKey();
					if(realItemStacks[slot] == null){
						realItemStacks[slot] = ii.getValue().getItemStack(player);
					}
				}
				
				player.getInventory().setContents(realItemStacks);
				player.updateInventory();
				
				if(InventoryAPIPlugin.playerInventories.get(player.getUniqueId()) != null){
					InventoryAPIPlugin.playerInventories.replace(player.getUniqueId(), inventoryID);
				}else{
					InventoryAPIPlugin.playerInventories.put(player.getUniqueId(), inventoryID);
				}
				player.closeInventory();
				return true;
			}else{
				InventoryAPIPlugin.getPlugin().getLogger().severe("Player object is null in setPlayerInventoryType.");
			}
		}
		
		return false;
	}
	
	public static boolean refreshPlayerView(Player player){
		if(player!=null){
			Inventory inv = InventoryAPIPlugin.inventories.get(InventoryAPIPlugin.playerInventories.get(player.getUniqueId()));
			ItemStack[] realItemStacks = new ItemStack[36];
			InventoryItem[] inventoryItems = inv.getAllInventoryItems();
			for(int i = 0; i < inventoryItems.length; i++){
				if(inventoryItems[i] != null){
					realItemStacks[i] = inventoryItems[i].getItemStack(player);
				}
			}
			
			for(Entry<Integer, InventoryItem> ii:InventoryAPIPlugin.globalItems.entrySet()){
				int slot = ii.getKey();
				if(realItemStacks[slot] == null){
					realItemStacks[slot] = ii.getValue().getItemStack(player);
				}
			}
			
			player.getInventory().setContents(realItemStacks);
			player.updateInventory();
			
			Menu menu = InventoryAPIPlugin.playerCurrentMenus.get(player);
			if(menu instanceof SubMenu && menu != null){
				SubMenuInvokingItemStack smiis = (SubMenuInvokingItemStack) menu.getInventoryItem();
				InventoryAPIPlugin.playerCurrentMenus.put(player.getUniqueId(), new SubMenu(player, smiis));
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean setPlayerArmorItem(Player player, ArmorType type, InventoryItem inventoryItem){//TODO handle closing and opening
		if(player != null){
			ArmorSet armor = InventoryAPIPlugin.playerArmor.get(player);
			if(armor != null){
				armor.setInventoryItem(type, inventoryItem);
				
				player.getInventory().setArmorContents(armor.getItemStacks());
				player.updateInventory();
				
				return true;
			}
		}else{
			InventoryAPIPlugin.getPlugin().getLogger().severe("Player object is null in setPlayerArmorItem.");
		}
		return false;
	}
	
	/***
	 * 
	 * @param secondaryViewLength Amount of inventory slots in secondary view (e.g. 54 for a double chest).
	 * @param rawSlot The raw slot value
	 * @return
	 */
	public static boolean isInPlayerInventorySection(int secondaryViewLength, int rawSlot){
		if(rawSlot >= secondaryViewLength){
			return true;
		}
		return false;
	}
	
	/***
	 * Returns the true slot value of a player inventory.
	 * @param xPos Slot position on x axis of main inventory section. This should be 1-9.
	 * @param yPos Slot position on y axis of main inventory section. This should be 1-4.
	 * @return The slot value.
	 * @exception Returns -1.
	 */
	public static int calculatePlayerInventorySlot(int xPos, int yPos){
		int slot = -1;
		
		if(yPos >= 1 && yPos <= 4 && xPos >= 1 && xPos <= 9){
			if (yPos == 4){
				slot = xPos - 1;
			}else{
				slot = 9 + (yPos * 9 - 9) + (xPos - 1); //9 is first position in top row
			}
		}
		
		return slot;
	}
	
	/***
	 * Returns the x axis position from a slot value of a player inventory.
	 * @param slot The slot of the item.
	 * @return The x axis slot value.
	 * @exception Returns -1.
	 */
	public static int getPlayerInventorySlotXPosition(int slot){
		int xslot = -1;
		
		if(slot >= 0 && slot < 36){
			xslot = (slot % 9) + 1;
		}
		
		return xslot;
	}
	
	/***
	 * Returns the y axis position from a slot value of a player inventory.
	 * @param slot The slot of the item.
	 * @return The y axis slot value.
	 * @exception Returns -1.
	 */
	public static int getPlayerInventorySlotYPosition(int slot){
		int yslot = -1;
		
		if(slot<9){
			yslot = 4;
		}else if(slot < 36){
			yslot = (slot - (slot % 9)) / 9;
		}
		
		return yslot;
	}
	
	/***
	 * Returns the true slot value of a chest inventory.
	 * @param xPos Slot position on x axis of main inventory section. This should be 1-9.
	 * @param yPos Slot position on y axis of main inventory section. This could be 1-3 or 1-6 depending on chest size.
	 * @return The slot value.
	 * @exception Returns -1.
	 */
	public static int calculateChestInventorySlot(int xPos, int yPos){
		int slot = -1;
		
		if(yPos >= 1 && yPos <= 6 && xPos >= 1 && xPos <= 9){
			slot = (yPos * 9 - 9) + (xPos - 1); //9 is first position in top row
		}
		
		return slot;
	}
	
	/***
	 * Gets the id assigned to the input player's UUID.
	 * @param playerUUID
	 * @return Integer form of inventoryID. By default, if a player has not been assigned an inventory this will assign them default inventory 0 and return as such.
	 */
	public static Integer getPlayerInventoryID(UUID playerUUID){
		Integer inventoryID = InventoryAPIPlugin.playerInventories.get(playerUUID);
		
		if(inventoryID == null){
			InventoryAPIPlugin.playerInventories.put(playerUUID, 0);
			inventoryID = 0;
		}
		
		return inventoryID;
	}
	
	/***
	 * Generates a simple command executor itemstack (ActionItemStack)
	 * @param inventoryID The ID of the inventory type that this itemstack resides in.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 */
	public static void addItemToInventory(int inventoryID, ItemStack itemStack, int xPos, int yPos, String command, boolean requiresConfirmation){
		ActionItemStack apiItemStack = new ActionItemStack(itemStack, command, requiresConfirmation);
		InventoryAPIPlugin.inventories.get(inventoryID).addToInventory(apiItemStack, xPos, yPos);
	}
	
	/***
	 * Generates a command executor itemstack (ActionItemStack) with an ItemStackGetter interface
	 * @param inventoryID The ID of the inventory type that this itemstack resides in.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 */
	public static void addItemToInventory(int inventoryID, ItemStack itemStack, int xPos, int yPos, String command, boolean requiresConfirmation, ItemStackGetter getter){
		ActionItemStack apiItemStack = new ActionItemStack(itemStack, command, requiresConfirmation, getter);
		InventoryAPIPlugin.inventories.get(inventoryID).addToInventory(apiItemStack, xPos, yPos);
	}
	
	/***
	 * Generates a PlayerSearchInvokingItemStack that gathers its uuids for browsing from a redis set.
	 * @param inventoryID The ID of the inventory type that this itemstack resides in.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 * @param replacementString The unique string in the command that should be replaced with the result UUID.
	 * @param redisSetKey The set in redis containing uuids to be browsed by the player.
	 * @param excludeViewerUUID Whether or not the returned set should exclude the inventory viewer's UUID.
	 */
	public static void addItemToInventory(int inventoryID, ItemStack itemStack, int xPos, int yPos, String command, String replacementString, String redisSetKey, boolean excludeViewerUUID, boolean requiresConfirmation){
		PlayerSearchInvokingItemStack apiItemStack = new PlayerSearchInvokingItemStack(itemStack, command, replacementString, redisSetKey, excludeViewerUUID, requiresConfirmation);
		InventoryAPIPlugin.inventories.get(inventoryID).addToInventory(apiItemStack, xPos, yPos);
	}
	
	/***
	 * Generates a PlayerSearchInvokingItemStack that gathers its uuids for browsing from the results of an SQL query.
	 * @param inventoryID The ID of the inventory type that this itemstack resides in.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 * @param replacementString The unique string in the command that should be replaced with the result UUID.
	 * @param sqlString The SQL command that will generate a resultset containing the needed UUIDs.
	 * @param sqlUUIDColumn The column to retrieve UUIDs to be browsed by the player.
	 * @param usesOriginCode Whether to replace string $ORIGIN with item user's uuid.
	 */
	public static void addItemToInventory(int inventoryID, ItemStack itemStack, int xPos, int yPos, String command, String replacementString, String sqlString, String sqlUUIDColumn, boolean usesOriginCode, boolean requiresConfirmation){
		PlayerSearchInvokingItemStack apiItemStack = new PlayerSearchInvokingItemStack(itemStack, command, replacementString, sqlString, sqlUUIDColumn, usesOriginCode, requiresConfirmation);
		InventoryAPIPlugin.inventories.get(inventoryID).addToInventory(apiItemStack, xPos, yPos);
	}
	
	/***
	 * Generates a SubMenuInvokingItemStack
	 * @param inventoryID The ID of the inventory type that this itemstack resides in.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param inventoryType The type of inventory to be displayed on item use.
	 * @param containedItems The InventoryItems contained within
	 * @param inventoryName
	 */
	public static SubMenuInvokingItemStack addItemToInventory(int inventoryID, ItemStack itemStack, int xPos, int yPos, int inventorySize, InventoryItem[] containedItems, String inventoryName){
		SubMenuInvokingItemStack apiItemStack = new SubMenuInvokingItemStack(itemStack, inventorySize, containedItems, inventoryName);
		InventoryAPIPlugin.inventories.get(inventoryID).addToInventory(apiItemStack, xPos, yPos);
		return apiItemStack;
	}
	
	/***
	 * Generates a simple command executor itemstack (ActionItemStack)
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 * @param inventoryIDs The IDs of the inventories that this itemstack resides in.
	 */
	public static void addItemToInventories(ItemStack itemStack, int xPos, int yPos, String command, boolean requiresConfirmation, int... inventoryIDs){
		ActionItemStack apiItemStack = new ActionItemStack(itemStack, command, requiresConfirmation);
		for (int i = 0; i < inventoryIDs.length; ++i){
			InventoryAPIPlugin.inventories.get(inventoryIDs[i]).addToInventory(apiItemStack, xPos, yPos);
		}
	}
	
	/***
	 * Generates a PlayerSearchInvokingItemStack that gathers its uuids for browsing from a redis set.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 * @param replacementString The unique string in the command that should be replaced with the result UUID.
	 * @param redisSetKey The set in redis containing uuids to be browsed by the player.
	 * @param excludeViewerUUID Whether or not the returned set should exclude the inventory viewer's UUID.
	 * @param inventoryIDs The IDs of the inventories that this itemstack resides in.
	 */
	public static void addItemToInventories(ItemStack itemStack, int xPos, int yPos, String command, String replacementString, String redisSetKey, boolean excludeViewerUUID, boolean requiresConfirmation, int... inventoryIDs){
		PlayerSearchInvokingItemStack apiItemStack = new PlayerSearchInvokingItemStack(itemStack, command, replacementString, redisSetKey, excludeViewerUUID, requiresConfirmation);
		for (int i = 0; i < inventoryIDs.length; ++i){
			InventoryAPIPlugin.inventories.get(inventoryIDs[i]).addToInventory(apiItemStack, xPos, yPos);
		}
	}
	
	/***
	 * Generates a PlayerSearchInvokingItemStack that gathers its uuids for browsing from the results of an SQL query.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param command The command to execute when this item is interacted with.
	 * @param replacementString The unique string in the command that should be replaced with the result UUID.
	 * @param sqlString The SQL command that will generate a resultset containing the needed UUIDs.
	 * @param sqlUUIDColumn The column to retrieve UUIDs to be browsed by the player.
	 * @param usesOriginCode Whether to replace string $ORIGIN with item user's uuid.
	 * @param inventoryIDs The IDs of the inventories that this itemstack resides in.
	 */
	public static void addItemToInventories(ItemStack itemStack, int xPos, int yPos, String command, String replacementString, String sqlString, String sqlUUIDColumn, boolean usesOriginCode, boolean requiresConfirmation, int... inventoryIDs){
		PlayerSearchInvokingItemStack apiItemStack = new PlayerSearchInvokingItemStack(itemStack, command, replacementString, sqlString, sqlUUIDColumn, usesOriginCode, requiresConfirmation);
		for (int i = 0; i < inventoryIDs.length; ++i){
			InventoryAPIPlugin.inventories.get(inventoryIDs[i]).addToInventory(apiItemStack, xPos, yPos);
		}
	}
	
	/***
	 * Generates a SubMenuInvokingItemStack
	 * @param inventoryID The ID of the inventory type that this itemstack resides in.
	 * @param itemStack Vanilla ItemStack to display in specified slot of player inventory.
	 * @param xPos X-axis position of itemstack in player inventory. This must be in the range 1-9. This range is from left to right of the inventory.
	 * @param yPos Y-axis position of itemstack in player inventory. This must be in the range 1-4. This range is from top to bottom of the inventory.
	 * @param inventoryType The type of inventory to be displayed on item use.
	 * @param containedItems The InventoryItems contained within
	 * @param inventoryName
	 */
	public static SubMenuInvokingItemStack addItemToInventories(ItemStack itemStack, int xPos, int yPos, int inventorySize, InventoryItem[] containedItems, String inventoryName, int... inventoryIDs){
		SubMenuInvokingItemStack apiItemStack = new SubMenuInvokingItemStack(itemStack, inventorySize, containedItems, inventoryName);
		for (int i = 0; i < inventoryIDs.length; ++i){
			InventoryAPIPlugin.inventories.get(inventoryIDs[i]).addToInventory(apiItemStack, xPos, yPos);
		}
		return apiItemStack;
	}
	
	public static void addGlobalInventoryItem(InventoryItem inventoryItem, int xPos, int yPos){
		int slot = calculatePlayerInventorySlot(xPos, yPos);
		if(!InventoryAPIPlugin.globalItems.containsKey(slot) && slot >= 0 && slot < 36){
			InventoryAPIPlugin.globalItems.put(calculatePlayerInventorySlot(xPos, yPos), inventoryItem);
		}
	}
}
