package network.marble.inventoryapi.listeners;

import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.entity.SheepDyeWoolEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import network.marble.inventoryapi.InventoryAPIPlugin;
import network.marble.inventoryapi.api.InventoryAPI;
import network.marble.inventoryapi.inventories.ConfirmationMenu;
import network.marble.inventoryapi.inventories.Inventory;
import network.marble.inventoryapi.inventories.Menu;
import network.marble.inventoryapi.inventories.PlayerListMenu;
import network.marble.inventoryapi.inventories.SubMenu;
import network.marble.inventoryapi.itemstacks.ActionItemStack;
import network.marble.inventoryapi.itemstacks.InventoryItem;
import network.marble.inventoryapi.itemstacks.PlayerSearchInvokingItemStack;
import network.marble.inventoryapi.itemstacks.SubMenuInvokingItemStack;

public class InventoryListener implements Listener {
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLogin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		InventoryAPIPlugin.playerLastItemUse.putIfAbsent(player.getUniqueId(), 0L);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerLeave(PlayerQuitEvent event){
		UUID uuid = event.getPlayer().getUniqueId();
		
		InventoryAPIPlugin.playerInventories.remove(uuid);
		InventoryAPIPlugin.playerCurrentMenus.remove(uuid);
		InventoryAPIPlugin.playerLastItemUse.remove(uuid);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInventoryClose(InventoryCloseEvent event){
		Player player = (Player) event.getPlayer();
		UUID uuid = player.getUniqueId();
		Menu menu = InventoryAPIPlugin.playerCurrentMenus.get(uuid);
		if(menu instanceof PlayerListMenu){
			Inventory inv = InventoryAPIPlugin.inventories.get(InventoryAPIPlugin.playerInventories.get(uuid));
			player.getInventory().setContents(inv.getRealInventory());
			player.updateInventory();
		}
		
		InventoryAPIPlugin.playerCurrentMenus.remove(uuid);
	}
		
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemClick(InventoryClickEvent event) {
		event.getRawSlot();
		if(event.getSlot() >= 0){//Protect against clicks from outside of inventory window
			activateItemEffect(event.getWhoClicked(), event.getInventory(), event.getSlot(), event.getCurrentItem(), event.getRawSlot());
		}
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemUse(PlayerInteractEvent event) {
		Action action = event.getAction();
		boolean cancel = true;
		if(action == Action.RIGHT_CLICK_BLOCK || action ==  Action.LEFT_CLICK_BLOCK){
			switch(event.getClickedBlock().getType()){
				case ACACIA_DOOR: cancel = false; break;
				case BED_BLOCK: cancel = false; break;
				case BIRCH_DOOR: cancel = false; break;
				case BREWING_STAND: cancel = false; break;
				case BURNING_FURNACE: cancel = false; break;
				case CHEST: cancel = false; break;
				case DARK_OAK_DOOR: cancel = false; break;
				case ENCHANTMENT_TABLE: cancel = false; break;
				case FENCE_GATE: cancel = false; break;
				case FURNACE: cancel = false; break;
				case JUKEBOX: cancel = false; break;
				case JUNGLE_DOOR: cancel = false; break;
				case LEVER: cancel = false; break;
				case NOTE_BLOCK: cancel = false; break;
				case SPRUCE_DOOR: cancel = false; break;
				case STONE_BUTTON: cancel = false; break;
				case TRAPPED_CHEST: cancel = false; break;
				case TRAP_DOOR: cancel = false; break;
				case TRIPWIRE: cancel = false; break;
				case WOOD_BUTTON: cancel = false; break;
				case WOODEN_DOOR: cancel = false; break;
				case WORKBENCH: cancel = false; break;
				default: break;
			}
		}
		
		if(action == Action.PHYSICAL){
			switch(event.getClickedBlock().getType()){
				case WOOD_PLATE: cancel = false; break;
				case STONE_PLATE: cancel = false; break;
				case IRON_PLATE: cancel = false; break;
				case GOLD_PLATE: cancel = false; break;
				default: break;
			}
		}else if(cancel){
			PlayerInventory inv = event.getPlayer().getInventory();
			int slot = inv.getHeldItemSlot();
			activateItemEffect(event.getPlayer(), inv, slot, inv.getItem(slot), slot);
		}
		
		event.setCancelled(cancel);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemFrameInteraction(PlayerInteractEntityEvent event){
		if(event.getRightClicked() instanceof ItemFrame){
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDropItem(PlayerDropItemEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemPickup(PlayerPickupItemEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPlace(BlockPlaceEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onSwapHand(PlayerSwapHandItemsEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemUse(InventoryPickupItemEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntityType().equals(EntityType.PLAYER)){
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onHangingBreak(HangingBreakEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onHangingPlace(HangingPlaceEvent event) {
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerShearEntity(PlayerShearEntityEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onSheepDyeWool(SheepDyeWoolEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onVehicleDestroy(VehicleDestroyEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityTame(EntityTameEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityShootBow(EntityShootBowEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPortalCreate(PortalCreateEvent event){
		event.setCancelled(true);
	}
	
	private void activateItemEffect(LivingEntity livingEntity, org.bukkit.inventory.Inventory inventory, int slot, ItemStack itemStack, int rawSlot){
		if(livingEntity instanceof Player){
			Player player = (Player) livingEntity;
			UUID playerUUID = player.getUniqueId();
			if(System.currentTimeMillis() > InventoryAPIPlugin.playerLastItemUse.get(playerUUID) + InventoryAPIPlugin.getConfigModel().clickDelay){
				InventoryAPIPlugin.playerLastItemUse.replace(playerUUID, System.currentTimeMillis());
				InventoryType type = inventory.getType();
				Menu currentMenu = InventoryAPIPlugin.playerCurrentMenus.get(playerUUID);
				boolean currentMenuIsSubMenu = currentMenu instanceof SubMenu;
				if((type == InventoryType.CRAFTING || type == InventoryType.CREATIVE || type == InventoryType.PLAYER || currentMenuIsSubMenu || InventoryAPI.isInPlayerInventorySection(inventory.getSize(), rawSlot)) && slot <= 35){//TODO ignore armour or something
					Integer invID = InventoryAPIPlugin.playerInventories.get(playerUUID);
					if ((invID != null && !currentMenuIsSubMenu) || (currentMenuIsSubMenu && rawSlot >= currentMenu.getInventorySize())){//if the player has an assigned inventory ID
						if(!(currentMenu instanceof PlayerListMenu)){
							Inventory inv = InventoryAPIPlugin.inventories.get(invID);
							InventoryItem invItem = inv.getInventoryItem(slot);
							
							processItemStack(invItem, itemStack, playerUUID, player, currentMenu, slot);
						}else{
							((PlayerListMenu)currentMenu).performAction(itemStack);
						}
					}else if(currentMenuIsSubMenu){
						SubMenu sm = (SubMenu)currentMenu;
						SubMenuInvokingItemStack smiis = (SubMenuInvokingItemStack)sm.getInventoryItem();
						processItemStack(smiis.getContainedItem(rawSlot), itemStack, playerUUID, player, currentMenu, slot);
					}
				}else if(type == InventoryType.CHEST){
					ItemMeta itemMeta = itemStack.getItemMeta();
					if(itemMeta != null && currentMenu != null){
						if(currentMenu instanceof PlayerListMenu){
							PlayerListMenu plm = (PlayerListMenu)currentMenu;
							PlayerSearchInvokingItemStack stack = (PlayerSearchInvokingItemStack)plm.getInventoryItem();
							List<String> lore = itemMeta.getLore();
							if(lore!=null){
								if(lore.size()>0){
									String command = stack.getReplacedCommand(lore.get(0));
									if(command != null){
										if(stack.requiresConfirmation()){
											InventoryAPI.closePlayerCurrentMenu(player);
											
											InventoryAPIPlugin.playerCurrentMenus.put(playerUUID, new ConfirmationMenu(player, stack, command));
										}else{
											try{
												InventoryAPIPlugin.getPlugin().getServer().dispatchCommand(player, command);
											}catch(Exception e){
												e.printStackTrace();
											}
										}
									}
									plm.localRefresh();
								}else{
									InventoryAPIPlugin.getPlugin().getLogger().info("No player head selected.");
								}
							}
						}
					}
				}else if (type == InventoryType.HOPPER){
					if(currentMenu instanceof ConfirmationMenu){
						ConfirmationMenu menu = (ConfirmationMenu) currentMenu;
						if(itemStack != null){
							if(itemStack.getType() == Material.EMERALD_BLOCK){
								menu.agreeToExecute();
							}else{
								menu.disagreeToExecute();
							}
						}
					}
				}
			}
		}
	}
	
	private void processItemStack(InventoryItem invItem, ItemStack itemStack, UUID playerUUID, Player player, Menu currentMenu, int slot){
		InventoryAPIPlugin.getPlugin().getLogger().info("reached processor");
		if(invItem == null){//if the item in the stored InvAPI Inventory is not null
			invItem = InventoryAPIPlugin.globalItems.get(slot);
			if(invItem == null){
				return;
			}
		}
		ItemStack invStack = invItem.getItemStack(player);//TODO check
		if(invStack.getType().equals(itemStack.getType())){
			if(invItem instanceof ActionItemStack){
				ActionItemStack ais = (ActionItemStack) invItem;
				String command = ais.getCommand();
				if(command != null){
					if(ais.requiresConfirmation()){
						InventoryAPI.closePlayerCurrentMenu(player);
						
						InventoryAPIPlugin.playerCurrentMenus.put(playerUUID, new ConfirmationMenu(player, invItem, command));
					}else{
						try{
							InventoryAPIPlugin.getPlugin().getServer().dispatchCommand(player, command);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				if(player.getOpenInventory() != null && !ais.requiresConfirmation() && !(currentMenu instanceof PlayerListMenu)){
					player.closeInventory();
				}
			}else{
				if(InventoryAPIPlugin.playerCurrentMenus.get(playerUUID)!=null){
					player.closeInventory();
				}
				if(invItem instanceof PlayerSearchInvokingItemStack){
					PlayerSearchInvokingItemStack psiis = (PlayerSearchInvokingItemStack) invItem;
					InventoryAPIPlugin.playerCurrentMenus.put(playerUUID, new PlayerListMenu(player, psiis, psiis.getUUIDList(player)));
				}else if(invItem instanceof SubMenuInvokingItemStack){
					SubMenuInvokingItemStack smiis = (SubMenuInvokingItemStack)invItem;
					InventoryAPIPlugin.playerCurrentMenus.put(playerUUID, new SubMenu(player, smiis));
				}
			}
		}
	}
}
