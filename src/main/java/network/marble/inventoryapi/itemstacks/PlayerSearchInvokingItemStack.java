package network.marble.inventoryapi.itemstacks;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import network.marble.inventoryapi.InventoryAPIPlugin;
import redis.clients.jedis.Jedis;

public class PlayerSearchInvokingItemStack extends InventoryItem{
	String command;
	String replaceString;
	boolean isRedis;
	boolean excludeViewerUUID;
	
	String sqlString;
	String setName;
	
	boolean usesOriginCode;
	
	boolean requiresConfirmation;
	
	public PlayerSearchInvokingItemStack(ItemStack itemStack, String command, String replaceString, String sqlString, String sqlUUIDColumn, boolean usesOriginCode, boolean requiresConfirmation) {
		super(itemStack);
		this.command = command;
		this.replaceString = replaceString;
		
		isRedis = false;
		this.sqlString = sqlString;
		setName = sqlUUIDColumn;
		this.usesOriginCode = usesOriginCode;
		this.requiresConfirmation = requiresConfirmation;
	}
	
	public PlayerSearchInvokingItemStack(ItemStack itemStack, String command, String replaceString, String redisSetKey, boolean excludeViewerUUID, boolean requiresConfirmation) {
		super(itemStack);
		this.command = command;
		this.replaceString = replaceString;
		
		isRedis = true;
		setName = redisSetKey;
		this.excludeViewerUUID = excludeViewerUUID;
		this.requiresConfirmation = requiresConfirmation;
	}
	
	public String getRawCommand(){
		return command;
	}
	
	public boolean requiresConfirmation(){
		return requiresConfirmation;
	}
	
	public String getReplacedCommand(String input){
		String output = command;
		output = output.replaceAll(replaceString, input);
		return output;
	}
	
	public String getReplaceString(){
		return replaceString;
	}
	
	public String getSQLString(){
		return sqlString;
	}
	
	public String getsetName(){
		return setName;
	}
	
	public boolean isRedis(){
		return isRedis;
	}
	
	public boolean usesOriginCode(){
		return usesOriginCode;
	}
	
	public boolean doesExcludeViewerUUID(){
		return excludeViewerUUID;
	}
	
	public UUID[] getUUIDList(Player player){
		UUID[] uuids;
		if(isRedis){
			Set<String> stringUUIDs = null;
			try (Jedis jedis = InventoryAPIPlugin.pool.getResource()){
				stringUUIDs = jedis.sinter(setName);
				uuids = new UUID[stringUUIDs.size()];//Will leave a null space at the end if we're 
				String viewerUUID = player.getUniqueId().toString();
				Iterator<String> uuidIterator = stringUUIDs.iterator();
				int i = 0;
				boolean excluded = false;
				while(uuidIterator.hasNext()){
					String uuidRawKey = uuidIterator.next();
					String[] splitKey = uuidRawKey.split(":");
					if(excludeViewerUUID && !excluded){
						if(splitKey[splitKey.length-1].equals(viewerUUID)){
							excluded = true;
							continue;
						}
					}
					uuids[i] = UUID.fromString(splitKey[splitKey.length-1]);
					i++;
			     }
    			return uuids;
    		}
		}else{
			try{
				List<String> preprocessedUUIDS = new ArrayList<String>();
				PreparedStatement pst;
				if(usesOriginCode){
					String updatedSQLString = sqlString.replaceAll("\\$ORIGIN", player.getUniqueId().toString());
					pst = InventoryAPIPlugin.conn.prepareStatement(updatedSQLString);
				}else{
					pst = InventoryAPIPlugin.conn.prepareStatement(sqlString);
				}
	    		
	    		ResultSet rs = pst.executeQuery();
	    		while(rs.next()){
	    			preprocessedUUIDS.add(rs.getString(setName));
	    			
	    		}
	    		pst.close();
	    		rs.close();
	    		
	    		uuids = new UUID[preprocessedUUIDS.size()];
	    		for(int i = 0; i < preprocessedUUIDS.size(); ++i){
	    			uuids[i] = UUID.fromString(preprocessedUUIDS.get(i));
	    		}
	    		return uuids;
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
		}
		return null;
	}
	
}
