package network.marble.inventoryapi.itemstacks;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SubMenuInvokingItemStack extends InventoryItem{
	private InventoryItem[] containedItems;
	private ItemStack itemStack;
	private String inventoryName;
	private ItemStack[] realInventory;
	int inventorySize;
	
	
	public SubMenuInvokingItemStack(ItemStack itemStack, int inventorySize, InventoryItem[] containedItems, String inventoryName) {
		super(itemStack);
		this.itemStack = itemStack;
		this.inventorySize = inventorySize;
		this.containedItems = containedItems;
		this.inventoryName = inventoryName;
		
		realInventory = new ItemStack[containedItems.length];
		for(int i = 0; i < containedItems.length; ++i){
			if(containedItems[i] != null){
				realInventory[i] = containedItems[i].getItemStack(null);
			}
		}
	}
	
	public boolean insertInventoryItem(InventoryItem inventoryItem, int slot){
		if(inventoryItem != null && slot >=0 && slot < inventorySize){
			if(containedItems[slot] == null){
				if(realInventory[slot] == null){
					containedItems[slot] = inventoryItem;
					realInventory[slot] = inventoryItem.getItemStack(null);
					return true;
				}
			}
		}
		
		return false;
	}
	
	public InventoryItem getContainedItem(int slot){
		return containedItems[slot];
	}
	
	public InventoryItem[] getContainedItems(){
		return containedItems;
	}
	
	public ItemStack getOriginItemStack(){
		return itemStack;
	}
	
	public String getInventoryName(){
		return inventoryName;
	}

	public ItemStack[] getRealInventory(Player player) {
		if(player == null){
			return realInventory;
		}else{
			ItemStack[] playerItems = new ItemStack[realInventory.length];
			for(int i = 0; i < containedItems.length; ++i){
				if(containedItems[i] != null){
					playerItems[i] = containedItems[i].getItemStack(player);
				}
			}
			
			return playerItems;
		}
	}
	
	public int getInventorySize() {
		return inventorySize;
	}

}
