package network.marble.inventoryapi.itemstacks;

import org.bukkit.inventory.ItemStack;

import network.marble.inventoryapi.interfaces.ItemStackGetter;

public class ActionItemStack extends InventoryItem{
	private String command;
	boolean requiresConfirmation;
	
	public ActionItemStack(ItemStack itemStack, String command, boolean requiresConfirmation) {
		super(itemStack);
		this.command = command;
		this.requiresConfirmation = requiresConfirmation;
	}
	
	public ActionItemStack(ItemStack itemStack, String command, boolean requiresConfirmation, ItemStackGetter getter) {
		super(itemStack, getter);
		this.command = command;
		this.requiresConfirmation = requiresConfirmation;
	}
	
	public String getCommand(){
		return command;
	}
	
	public boolean requiresConfirmation(){
		return requiresConfirmation;
	}

}
