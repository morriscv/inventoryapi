package network.marble.inventoryapi.itemstacks;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import network.marble.inventoryapi.interfaces.ItemStackGetter;

public abstract class InventoryItem {
	private ItemStack itemStack;
	private ItemStackGetter getter = null;
	private boolean usesGetter = false;
	
	public InventoryItem(ItemStack itemStack){
		this.itemStack = itemStack;
	}
	
	public InventoryItem(ItemStack itemStack, ItemStackGetter getter){
		this.itemStack = itemStack;
		this.getter = getter;
		this.usesGetter = true;
	}
	
	public ItemStack getItemStack(Player player){
		if(!usesGetter || player == null){
			return itemStack;
		}else{
			ItemStack newStack = getter.getItemStack(this, player);
			if(newStack!=null){
				return newStack;
			}
		}
		return itemStack;
	}
	
	public void setItemStack(ItemStack stack){
		itemStack = stack;
	}
}
