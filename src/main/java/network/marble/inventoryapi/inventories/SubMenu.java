package network.marble.inventoryapi.inventories;

import org.bukkit.entity.Player;
import network.marble.inventoryapi.InventoryAPIPlugin;
import network.marble.inventoryapi.itemstacks.SubMenuInvokingItemStack;


public class SubMenu extends Menu{

	public SubMenu(Player targetPlayer, SubMenuInvokingItemStack sourceItem) {
		super(targetPlayer, sourceItem, sourceItem.getInventorySize());
		
		org.bukkit.inventory.Inventory inv = InventoryAPIPlugin.getPlugin().getServer().createInventory(null, sourceItem.getInventorySize(), sourceItem.getInventoryName());
		inv.setContents(sourceItem.getRealInventory(targetPlayer));
		targetPlayer.openInventory(inv);
	}

}
