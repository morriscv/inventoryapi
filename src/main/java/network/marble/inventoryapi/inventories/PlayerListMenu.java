package network.marble.inventoryapi.inventories;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import net.md_5.bungee.api.ChatColor;
import network.marble.inventoryapi.InventoryAPIPlugin;
import network.marble.inventoryapi.itemstacks.InventoryItem;
import network.marble.inventoryapi.itemstacks.PlayerSearchInvokingItemStack;
import redis.clients.jedis.Jedis;

public class PlayerListMenu extends Menu{
	private int page = 1;
	Inventory inv;
	TreeMap<String,String> sortedPairs = new TreeMap<String,String>(String.CASE_INSENSITIVE_ORDER);
	
	public PlayerListMenu(Player targetPlayer, InventoryItem inventoryItem, UUID[] playerUUIDs) {
		super(targetPlayer, inventoryItem, 54);
		
		generateSortedPairs(playerUUIDs);
		
		inv = InventoryAPIPlugin.getPlugin().getServer().createInventory(null, 54, "Select A Player:");//Generate double chest inventory
		inv.setContents(buildInventoryPage(page));
		
		network.marble.inventoryapi.inventories.Inventory pinv = InventoryAPIPlugin.inventories.get(-1);
		targetPlayer.getInventory().setContents(pinv.getRealInventory());
		targetPlayer.updateInventory();
		targetPlayer.openInventory(inv);
	}
	
	private void generateSortedPairs(UUID[] playerUUIDs){
		sortedPairs.clear();
		try(Jedis jedis = InventoryAPIPlugin.pool.getResource()){//TODO async
			for(int i = 0; i < playerUUIDs.length; ++i){
				if(playerUUIDs[i] != null){
					String uuid = playerUUIDs[i].toString();
					String keyName = "username:" + uuid;//Key that should contain
					
					if(jedis.exists(keyName)){
						String username = jedis.get(keyName);
						sortedPairs.put(username, uuid);
					}
				}
			}
		}
	}
	
	private ItemStack[] buildInventoryPage(int page){
		ItemStack[] is = new ItemStack[54];
		
		int startPosition = page * 54 - 54;
		
		List<String> indexes = new ArrayList<String>(sortedPairs.keySet());
		if(indexes.size() > startPosition){
			String key = indexes.get(startPosition);
			
			int i = 0;
			for(Entry<String, String> entry: sortedPairs.tailMap(key).entrySet()){
				if(i>53){
					break;
				}
				String uuidString = entry.getValue();
				String trueUser = entry.getKey();
				
				//Create meta and set skull to look like player
				SkullMeta meta = (SkullMeta)Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
				meta.setOwner(trueUser);
				//Display username as item name
				meta.setDisplayName(ChatColor.GOLD + trueUser);
				//Set lore to UUID
				List<String> lore = new ArrayList<String>();
		        lore.add(uuidString);
		        meta.setLore(lore);
		        //Apply meta
				ItemStack stack = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.PLAYER.ordinal());
				stack.setItemMeta(meta);
				
				is[i] = stack;
				i++;
			}
			
		}else{
			if(page!=1){
				return null;
			}
		}
		
		return is;
	}
	
	public void localRefresh(){
		InventoryItem ii = getInventoryItem();
		if(ii instanceof PlayerSearchInvokingItemStack){
			if(!((PlayerSearchInvokingItemStack) ii).requiresConfirmation()){
				inv.setContents(buildInventoryPage(page));
				this.getTargetPlayer().updateInventory();
			}
		}
	}
	
	private void dataRefresh(){
		generateSortedPairs(((PlayerSearchInvokingItemStack)this.getInventoryItem()).getUUIDList(this.getTargetPlayer()));
		changePage(1, true);
	}
	
	private void changePage(int page, boolean noOptimisation){
		int modifiedPage = page;
		int lastPage = getLastPage();
		if(page>lastPage){
			modifiedPage = lastPage;
		}
		if(page < 1 || modifiedPage < 1){
			modifiedPage = 1;
		}
		if(modifiedPage != this.page || noOptimisation){
			ItemStack[] newContents = buildInventoryPage(modifiedPage);
			if(newContents != null){
				inv.setContents(newContents);
				this.getTargetPlayer().updateInventory();
				this.page = modifiedPage;
			}
		}
	}
	
	private int getLastPage(){
		boolean partialPage = sortedPairs.keySet().size() % 54 > 0;
		int basePage = (sortedPairs.keySet().size() - (sortedPairs.keySet().size() % 54))/54;
		if(partialPage){
			basePage+=1;
		}
		
		return basePage;
	}
	
	public void performAction(ItemStack itemStack){
		switch(itemStack.getType()){
			case SEEDS: changePage(1, false); break;
			case GOLD_BLOCK: changePage(page-100, false); break;
			case REDSTONE_BLOCK: changePage(page-10, false); break;
			case POTATO_ITEM: changePage(page-1, false); break;
			case NETHER_STAR: dataRefresh(); break;
			case BAKED_POTATO: changePage(page+1, false); break;
			case DIAMOND_BLOCK: changePage(page+10, false); break;
			case EMERALD_BLOCK: changePage(page+100, false); break;
			case EYE_OF_ENDER: changePage(getLastPage(), false); break;
			default: break;
		}
	}
}
