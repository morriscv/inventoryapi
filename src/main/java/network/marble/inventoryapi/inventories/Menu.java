package network.marble.inventoryapi.inventories;

import org.bukkit.entity.Player;

import network.marble.inventoryapi.itemstacks.InventoryItem;

public abstract class Menu {
	private Player targetPlayer;
	private InventoryItem inventoryItem;
	private int inventorySize;
	
	public Menu(Player targetPlayer, InventoryItem inventoryItem, int inventorySize){
		this.targetPlayer = targetPlayer;
		this.inventoryItem = inventoryItem;
		this.inventorySize = inventorySize;
	}
	
	public Player getTargetPlayer(){
		return targetPlayer;
	}
	
	public InventoryItem getInventoryItem(){
		return inventoryItem;
	}
	
	public int getInventorySize(){
		return inventorySize;
	}
}
