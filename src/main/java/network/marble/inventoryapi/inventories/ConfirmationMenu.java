package network.marble.inventoryapi.inventories;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import network.marble.inventoryapi.InventoryAPIPlugin;
import network.marble.inventoryapi.api.InventoryAPI;
import network.marble.inventoryapi.itemstacks.InventoryItem;

public class ConfirmationMenu extends Menu{
	String command;
	String actionDescription;
	
	/***
	 * Creates a yes/no dialogue for running the input command.
	 * @param targetPlayer Player to be confirming.
	 * @param inventoryItem Source InventoryItem.
	 * @param command Command to execute.
	 */
	public ConfirmationMenu(Player targetPlayer, InventoryItem inventoryItem, String command){//, String actionDescription) {
		super(targetPlayer, inventoryItem, 5);
		this.command = command;
		//this.actionDescription = actionDescription;
		
		ItemStack agree = new ItemStack(Material.EMERALD_BLOCK, 1, (short)0);
    	ItemMeta agreeMeta = agree.getItemMeta();
    	agreeMeta.setDisplayName(ChatColor.GREEN + "Yes");
    	//TODO action description
    	agree.setItemMeta(agreeMeta);
    	
    	ItemStack disagree = new ItemStack(Material.REDSTONE_BLOCK, 1, (short)0);
    	ItemMeta disagreeMeta = disagree.getItemMeta();
    	disagreeMeta.setDisplayName(ChatColor.RED + "No");
    	//TODO action description
    	disagree.setItemMeta(disagreeMeta);
		
    	ItemStack[] items = new ItemStack[5];
    	items[0] = agree;
    	items[4] = disagree;
		Inventory inv = InventoryAPIPlugin.getPlugin().getServer().createInventory(null, InventoryType.HOPPER, "Are you sure?");
		inv.setContents(items);
		targetPlayer.openInventory(inv);
	}
	
	public void agreeToExecute(){
		try{
			InventoryAPIPlugin.getPlugin().getServer().dispatchCommand(getTargetPlayer(), command);
		}catch(Exception e){
			e.printStackTrace();
		}
		InventoryAPI.closePlayerCurrentMenu(getTargetPlayer());
	}
	
	public void disagreeToExecute(){
		InventoryAPI.closePlayerCurrentMenu(getTargetPlayer());
	}
	
}
