package network.marble.inventoryapi.inventories;

import org.bukkit.inventory.ItemStack;

import network.marble.inventoryapi.enums.ArmorType;
import network.marble.inventoryapi.itemstacks.InventoryItem;

public class ArmorSet {
	
	InventoryItem[] inventoryItems = new InventoryItem[4];
	ItemStack[] items = new ItemStack[4];
	
	public void setInventoryItem(ArmorType type, InventoryItem inventoryItem){
		inventoryItems[type.getValue()] = inventoryItem;
		items[type.getValue()] = inventoryItem.getItemStack(null);
	}
	
	public InventoryItem getArmorInventoryItem(ArmorType type){
		return inventoryItems[type.getValue()];
	}
	
	public ItemStack[] getItemStacks(){
		return items;
	}
}
