package network.marble.inventoryapi;

public class InventoryConfig {
	public String redisHost = "127.0.0.1";
    public int redisPort = 6379;
	public String sqlHost = "178.32.10.184";
    public int sqlPort = 3306;
    public long clickDelay = 500L;
    public boolean enableInventoryProtection = true;
}
